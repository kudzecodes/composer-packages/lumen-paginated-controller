# Lumen Paginated Controller

## Description

This package adds 4 abstract controllers for you application to use:
1. `PaginatedController` - contains helpful utilities to build paginated controller.
2. `PaginatedUuidController` - same as `PaginatedController` but default order by value is `uuid`.
3. `PaginatedExportableController` - same as `PaginatedController` but contains validation utility for export request.
4. `PaginatedExportableUuidController` - sames as `PaginatedExportableController` but default order by value is `uuid`.

## Installation

1. Install this package as composer package into lumen project.