<?php

namespace Kudze\LumenPaginatedController\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;
use Kudze\LumenBaseController\Http\Controllers\Controller as BaseController;

abstract class PaginatedController extends BaseController
{
    const string ORDER_BY_ASC = 'asc';
    const string ORDER_BY_DESC = 'desc';

    protected int $defaultPageSize = 100;
    protected int $defaultPage = 1;
    protected string $defaultOrderBy = 'id';
    protected string $defaultOrderDirection = self::ORDER_BY_ASC;
    protected int $maxPageSize = 1000;

    /**
     * This should return valid orderBy values for the pagination route.
     */
    protected abstract function getValidOrderByKeys(): array;

    protected function getValidOrderByDirections(): array
    {
        return [self::ORDER_BY_ASC, self::ORDER_BY_DESC];
    }

    protected function getPageValidationRules(): string
    {
        return 'nullable|int|min:1';
    }

    protected function getPageSizeValidationRules(): string
    {
        return "nullable|int|min:1|max:$this->maxPageSize";
    }


    protected function getOrderByValidationRules(): array
    {
        return [
            'nullable',
            'string',
            new In($this->getValidOrderByKeys())
        ];
    }

    protected function getOrderByDirectionValidationRules(): array
    {
        return [
            'nullable',
            'string',
            new In($this->getValidOrderByDirections())
        ];
    }

    protected function getDefaultPaginatedValidationRules(): array
    {
        return [
            'page' => $this->getPageValidationRules(),
            'pageSize' => $this->getPageSizeValidationRules(),
            'orderBy' => $this->getOrderByValidationRules(),
            'orderByDirection' => $this->getOrderByDirectionValidationRules(),
        ];
    }

    protected function getDefaultPaginatedRequestArguments(): array
    {
        return [
            'page' => $this->defaultPage,
            'pageSize' => $this->defaultPageSize,
            'orderBy' => $this->defaultOrderBy,
            'orderByDirection' => $this->defaultOrderDirection,
        ];
    }

    /**
     * @throws ValidationException
     */
    protected function validatePaginatedRequest(Request $request, array $rules = []): array
    {
        return $this->validate($request, $rules + $this->getDefaultPaginatedValidationRules()) + $this->getDefaultPaginatedRequestArguments();
    }
}