<?php

namespace Kudze\LumenPaginatedController\Http\Controllers;

abstract class PaginatedUuidController extends PaginatedController
{
    protected string $defaultOrderBy = 'uuid';
}