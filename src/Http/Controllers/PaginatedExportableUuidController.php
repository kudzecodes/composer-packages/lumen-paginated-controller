<?php

namespace Kudze\LumenPaginatedController\Http\Controllers;

abstract class PaginatedExportableUuidController extends PaginatedExportableController
{
    protected string $defaultOrderBy = 'uuid';
}