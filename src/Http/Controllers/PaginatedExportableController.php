<?php

namespace Kudze\LumenPaginatedController\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

abstract class PaginatedExportableController extends PaginatedController
{
    protected function getDefaultExportValidationRules(): array
    {
        return [
            'orderBy' => $this->getOrderByValidationRules(),
            'orderByDirection' => $this->getOrderByDirectionValidationRules(),
        ];
    }

    protected function getDefaultExportRequestArguments(): array
    {
        return [
            'page' => null,
            'pageSize' => null,
            'orderBy' => $this->defaultOrderBy,
            'orderByDirection' => $this->defaultOrderDirection,
        ];
    }

    /**
     * @throws ValidationException
     */
    protected function validateExportRequest(Request $request, array $rules = []): array
    {
        return $this->validate($request, $rules + $this->getDefaultExportValidationRules()) + $this->getDefaultExportRequestArguments();
    }
}